<?php

namespace sirshark\myFirstPackage;

class myFirstPackage
{
    public function run(String $sName)
    {
        return 'Hi ' . $sName . '! How are you doing today?';
    }
}